class TeamController < ApplicationController
    def show
        @name = params[:id]
        @image = "/" + @name + ".png"

        if @name == "htmelodies"
            @prev = "bookhunter"
            @next = "gamehub"
        elsif @name == "gamehub"
            @prev = "htmelodies"
            @next = "findjobs"
        elsif @name == "findjobs"
            @prev = "gamehub"
            @next = "bookhunter"
        else # @name == "bookhunter"
            @prev = "findjobs"
            @next = "htmelodies"
        end
    end
end
