# Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
Rails.application.routes.draw do
  
  # Defines the root path route ("/")
  root "team#index"

  # Only use index and show action
  resources :team, only: [:index, :show]


  # Default route helper
  # Route helper: team_path(:id)
  # get '/team/:id', to: 'team#show', as: 'team'


  # User defined route helper using "as"
  # Route helper: project_path(:id)
  # get '/project/:id', to: 'team#show', as: 'project'


  # User defined route helper using "as"
  # Route helper: project_path()
  # get '/project', to: 'team#show', as: 'project'

end
